# Obligatorisk oppgave nr 2 i IN113
#
# Levert av: �yvind Nesse   (oyvindn@ifi.uio.no)
#            Frode Klevstul (frodekl@ifi.uio.no)
# 
# H�sten -96 (20.10.96)



# DELOPPGAVE A:

# oppgave A1: 

select distinct a.navn
from ansatt a, timelistelinje t, prosjekt p
where a.ansnr=t.ansatt and	
      t.prosjekt=p.prosjektnr and
      a.ansnr <> 33 and a.ansnr <> 104 and
      p.kunde in 
                  (select p.kunde
	          from prosjekt p, timelistelinje t
	          where t.prosjekt=p.prosjektnr and
	          (T.ansatt=33 or T.ansatt=104))
order by a.navn
go


# Resultat:

 navn                           
 ------------------------------ 
 Alen Milkovic                  
 Alsos, Hans Christian          
 Andreas Dahl                   
 Anne Merete Andersen           
 Anpalahan                      
 Arild Mikalsen                 
 Arne Nordvang                  
 Asbj�rn Danielsen              
 Audun Andersen                 
 Barkrakk og Drakk              
 Bente Aasgaard                 
 Bjarne Johannessen             
 Bj�rn Hansen                   
 Bj�rn Wangensteen              
 Christian H�e                  
 David A. Ranvig                
 Elin Bardal                    
 Elin V�ge Lafton               
 Erlend Olsen                   
 Eskil Teigen                   
 espenfj                        
 Espn Hang�rd                   
 Glenn Blandehoel               
 Hans Christian Kj�lberg        
 Hans-Petter Ruud               
 Heidi Bordell                  
 Henning Spjelkavik             
 Henrik Hahne                   
 Henrik Solgaard                
 Ingvild S�r�sen                
 Joachim Svendsen               
 Johan H. W. Basberg            
 Jon Andreas Larssen            
 Jon Harald Dunstr�m            
 J�rgen den rare                
 J�rn Blandehoel                
 Jos� L. Rojas                  
 Karl Erik Levik                
 Karlson P. Taket               
 Kim Sand                       
 Kjetil Pedersen                
 Knut Restad                   
 Kristin Nelvik                 
 Kristoffer Bergan              
 Lars N�ring                    
 Lars Preben Arnesen            
 Lene                           
 Lene Akerhaugen                
 m k juonolainen                
 Marianne Karlsen               
 Marit Moberg                   
 Mats 'The Wiz' Sollie          
 Mikkel Steine                  
 Mona Pettersen                 
 Nisse Lue                      
 Odd Hjelle                     
 Ola Presterud                  
 P�l Martin Bakken              
 Peder Stray                    
 Per Arne Karlsen               
 Per Thomas Jahr                
 Rafael Martinez                
 Ragnhild Kobro                 
 Rune Storl�pa                  
 Simen Gimle Hansen             
 Stian Andre Olsen              
 Svend everybody's friend       
 Sverre Steensen                
 THILEEPAN                      
 Thomas Landmark                
 Thomas Nicolaisen              
 Thorkild Stray                 
 Tor Hval                       
 Tor-�yvind Gundersen           
 Tora Kristine Ved�             
 Tore Lucky                     
 Torgeir Fritsvold              
 Torvalde E                     
 Vegard Hanssen                 
 
(79 rows affected)


# oppgave A2:

select distinct K.Navn,K.adresse
from Kunde K
Where k.kundenr not in( select k.kundenr
			from kunde k,prosjekt p,timelistelinje t
	                where k.kundenr=p.kunde and
		              p.prosjektnr=t.prosjekt and
                              (t.ansatt=33 or t.ansatt=104))
order by K.Navn,k.adresse
go


# Resultat:

 Navn                           adresse                                  
 ------------------------------ ---------------------------------------- 
 A/S EDB                        Svingen 65, Oslo                         
 Acme inc.                      Toon Town, California                    
 Ad Notam Gyldendal AS          Universitetsgt. 14, 0130 Oslo            
 Andeby Sosialkontor            Andebyvn 5c                              
 BB a/s                         St�lfj�ra 2                              
 Byblos briller                 En plass i Karl Johan                    
 CC a/s                         St�lfj�ra 3                              
 Chimaera Computing             Sinsenveien 13, 0101 OSLO                
 Cyberdyne ind.                 2351 LA                                  
 Dec                            Ingenstedsveien                          
 Delfinoppdrett AS              Sydney                                   
 Det Norske Veritas             1322 H�vik                               
 DetKongeligeSQLutviklingsfond  Slottsplassen 1                          
 Doffen                         DoffenVeien                              
 Dole                           DoleVeien                                
 Dust                           Pikk                                     
 Eirix                          2345 Milkyway                            
 EKSTRAKUNDE                    VEIEN 13                                 
 Elektronisk Informasjonsbehand Pilestredet 1                            
 Espen Hang�rd                  New York                                 
 F.Jompenisse                   Hakadal                                  
 Fiskemat for gule akvariefiske Marienlyst                               
 Foundation                     Valley                                   
 Free Software Foundation       http://www.gnu.org                       
 Gastro Compex                  Brager�ya 34, 1914 Enebakk               
 Geir Pluto & s�nn              21b Jokkmokk                             
 GravSoft                       Guller�sveien 26F                        
 Hagen transport A/S            1940 Hagan                               
 hans hansen                    sognsvn 218                              
 Hansen & Co                    lia 1, 0857 OSLO                         
 Idavoll                        Oslo Kretsfengsel                        
 jalla                          balla                                    
 James Bond                     (unknown)                                
 Karmoy Maakefabrikk            Haugesund                                
 Kjell Bordell stinker fis!     Vi vet hvem du er, nerd!!!               
 KKK                            Oslo                                     
 Kule K�re                      Toten                                    
 Leisure Suit Larry             c/o Sierra Online Inc., Seattle          
 makeITwork as                  Ekebergveien 224, Oslo                   
 NAS TEE                        m�rkem�rkem�rke-stedet 666               
 Nec                            Borte                                    
 NeverCom                       Oslo                                     
 Ole                            OleVeien                                 
 Onkel Skrue                    Pengebingen                              
 R. Imelig Grimm                I en hule, langt langt inne i skogen     
 Realistforeningens REGI        Mellom toalettene i underetasjen i VB    
 Robert E. Grant                Richmond, C.S.A.                         
 Room 101                       Dyrlandsveien 10, 1010 OSLO              
 S. Lem                         Store Stygge vei 96                      
 Skrekk og Gru                  Grusomheten 19, 1329 OSLO                
 slagteroluf                    vegetarianerd�dslisteveien 1             
 Start                          Kristiansand                             
 Statens Helsevesen             Sykdomsveien 1                           
 Stjel & R�m                    Skurkegata 10                            
 Telehor MArketing              Telenors lokaler, Grunerl�kka            
 THORS                          Dal                                      
 tut og kj�r                    brattbakken 3, 0123 Oslo                 
 Vegard Beider                  Kringsjaastyggebygg                      
 
(58 rows affected)


# oppgave A3:


 Select distinct A.Navn, Avd.navn
 from Ansatt A, Avdeling Avd
 where a.avdeling = avd.avdnr and
       a.ansnr in
	(select sjef
	 from ansatt)
        and
       a.ansnr not in
	(select  sjef
         from avdeling)
 order by A.Navn ,Avd.navn
go

     
# resultat: 

  Navn                           navn                           
 ------------------------------ ------------------------------ 
 Andreas Faafeng                Avdeling for tre-strukturer    
 Anpalahan                      Avdeling for slappe fisker     
 Birger Bugge Berger            Gruppe 8                       
 Bjarne Johannessen             Gruppe 2                       
 Bj�rn Wangensteen              Avdeling for slappe fisker     
 Catalin Togea                  Avdeling for ventende papirbun 
 Egil Stian Klokset             Gruppe 8                       
 Elin V�ge Lafton               Avdeling for ventende papirbun 
 espenfj                        Gruppe 8                       
 Heidi Bordell                  Avdeling for tre-strukturer    
 Henning Spjelkavik             Avdeling for ventende papirbun 
 Henrik Hahne                   Avdeling for tre-strukturer    
 Ingvild S�r�sen                Avdeling for hemmelige tjenest 
 Karl Erik Levik                Gruppe 5                       
 m k juonolainen                Avdeling for tre-strukturer    
 Mats 'The Wiz' Sollie          Avdeling for tre-strukturer    
 Nisse Lue                      Avdeling for hemmelige tjenest 
 Nisse Lue                      Avdeling for presis registreri 
 Nisse Lue                      Avdeling for tre-strukturer    
 Nisse Lue                      Avdeling for ventende papirbun 
 Nisse Lue                      Gruppe 2                       
 Nisse Lue                      Gruppe 5                       
 Nisse Lue                      Gruppe 8                       
 Rune Storl�pa                  Avdeling for tre-strukturer    
 THILEEPAN                      Avdeling for slappe fisker     
 Thorkild Stray                 Avdeling for tre-strukturer    
 Tor Hval                       Avdeling for ventende papirbun 
 Vidar, Eide                    Gruppe 8                       
 
(28 rows affected)


# oppgave A4:


Select distinct Avd.AvdNr,A2.Navn
from   Avdeling Avd ,Ansatt A1,Ansatt A2,Timelistelinje T
where  Avd.AvdNr=A1.Avdeling and
       A1.AnsNr=T.Ansatt and
       A2.Ansnr =Avd.sjef                   
group by Avd.AvdNr, A2.Navn 
order by sum(T.Timer) desc,Avd.AvdNr, A2.Navn 
go


# resultat:


 AvdNr Navn                           
 ----- ------------------------------ 
 7     hellel Bordell                 
 1     Arne Bjelle                    
 2     Bjorn Bordell                  
 4     Guro  Bordell                  
 8     Vidar Berget                   
 3     Fiun Bordell                   
 5     Julie Bordel                   
 6     Paal Bordell                   
 0     Heter ikke Kjell               
 
(9 rows affected)



# oppgave A5:


select distinct avd.avdnr,sum(t.timer),k.navn
from   avdeling avd,timelistelinje t,prosjekt p,ansatt a,kunde k
where  t.prosjekt=p.prosjektnr and
       a.ansnr=t.ansatt and
       a.avdeling=avd.avdnr and
       k.kundenr=p.kunde 
group  by k.navn,avd.avdnr
order by k.navn,avd.avdnr
go


# resultat:


 avdnr               navn                           
 -----  -----------  ------------------------------ 
 4               40  A/S EDB                        
 2               70  Acme inc.                      
 5               21  Acme inc.                      
 7               52  Acme inc.                      
 3               93  Ad Notam Gyldendal AS          
 0               66  Andeby Sosialkontor            
 1              299  Andeby Sosialkontor            
 2               47  Andeby Sosialkontor            
 3                6  Andeby Sosialkontor            
 4               19  Andeby Sosialkontor            
 5               51  Andeby Sosialkontor            
 6               71  Andeby Sosialkontor            
 7          6997814  Andeby Sosialkontor            
 2               20  BB a/s                         
 1              181  Byblos briller                 
 2               55  Byblos briller                 
 3              132  Byblos briller                 
 4               80  Byblos briller                 
 5               20  Byblos briller                 
 6              155  Byblos briller                 
 7              278  Byblos briller                 
 8              101  Byblos briller                 
 2               30  CC a/s                         
 5               76  Chimaera Computing             
 8               44  Chimaera Computing             
 2                3  Cyberdyne ind.                 
 4                9  Cyberdyne ind.                 
 7               49  Cyberdyne ind.                 
 8               61  Cyberdyne ind.                 
 0               58  Daffeposten A/S                
 1                7  Daffeposten A/S                
 2              822  Daffeposten A/S                
 3              135  Daffeposten A/S                
 4               77  Daffeposten A/S                
 5              212  Daffeposten A/S                
 6               36  Daffeposten A/S                
 7              116  Daffeposten A/S                
 8               67  Daffeposten A/S                
 1               18  Dec                            
 2              177  Dec                            
 4              361  Dec                            
 5               64  Dec                            
 6               41  Dec                            
 7                1  Dec                            
 8              232  Dec                            
 3              114  Delfinoppdrett AS              
 4               38  Delfinoppdrett AS              
 7               67  Delfinoppdrett AS              
 8              132  Delfinoppdrett AS              
 2             1000  Det Norske Veritas             
 3                9  Det Norske Veritas             
 4               26  Det Norske Veritas             
 5               28  Det Norske Veritas             
 7               43  Det Norske Veritas             
 0               16  DetKongeligeSQLutviklingsfond  
 1               72  DetKongeligeSQLutviklingsfond  
 6               63  DetKongeligeSQLutviklingsfond  
 7              820  DetKongeligeSQLutviklingsfond  
 8               20  DetKongeligeSQLutviklingsfond  
 1            60268  Doffen                         
 1               62  Dole                           
 3               60  Elektronisk Informasjonsbehand 
 2               29  F.Jompenisse                   
 3              102  F.Jompenisse                   
 4               24  F.Jompenisse                   
 5                7  F.Jompenisse                   
 7                8  F.Jompenisse                   
 1              194  Fiskemat for gule akvariefiske 
 2              253  Fiskemat for gule akvariefiske 
 3              150  Fiskemat for gule akvariefiske 
 4              182  Fiskemat for gule akvariefiske 
 5              180  Fiskemat for gule akvariefiske 
 6              530  Fiskemat for gule akvariefiske 
 7              243  Fiskemat for gule akvariefiske 
 8              557  Fiskemat for gule akvariefiske 
 2               47  Foundation                     
 3              104  Foundation                     
 4               68  Foundation                     
 5               37  Foundation                     
 6               36  Foundation                     
 7               12  Foundation                     
 7               24  Free Software Foundation       
 7               79  Gastro Compex                  
 8               46  Gastro Compex                  
 1               97  Geir Pluto & s�nn              
 2               37  Geir Pluto & s�nn              
 3               42  Geir Pluto & s�nn              
 4               29  Geir Pluto & s�nn              
 5               27  Geir Pluto & s�nn              
 7          7999845  Geir Pluto & s�nn              
 3               71  GravSoft                       
 1              216  Hagen transport A/S            
 2              777  Hagen transport A/S            
 3              854  Hagen transport A/S            
 4              344  Hagen transport A/S            
 5               34  Hagen transport A/S            
 6              276  Hagen transport A/S            
 7              270  Hagen transport A/S            
 8               93  Hagen transport A/S            
 6               59  Idavoll                        
 8               83  Idavoll                        
 1              648  Indigo                         
 2              763  Indigo                         
 3              754  Indigo                         
 4              366  Indigo                         
 5              705  Indigo                         
 6              845  Indigo                         
 7              626  Indigo                         
 8              695  Indigo                         
 1              271  James Bond                     
 2              273  James Bond                     
 3              178  James Bond                     
 4               22  James Bond                     
 6               22  James Bond                     
 7               65  James Bond                     
 1              239  Kalle nesevis                  
 2               83  Kalle nesevis                  
 3               46  Kalle nesevis                  
 4               42  Kalle nesevis                  
 5               72  Kalle nesevis                  
 6              231  Kalle nesevis                  
 7              160  Kalle nesevis                  
 8               50  Kalle nesevis                  
 2               38  Karmoy Maakefabrikk            
 3               51  Karmoy Maakefabrikk            
 6               70  Karmoy Maakefabrikk            
 8               16  Karmoy Maakefabrikk            
 5               10  Kjell Bordell stinker fis!     
 1              326  kjshghsgh                      
 2              110  kjshghsgh                      
 3               25  kjshghsgh                      
 5               14  kjshghsgh                      
 6               24  kjshghsgh                      
 8               14  kjshghsgh                      
 1               60  KKK                            
 0               25  Kule K�re                      
 1              214  Kule K�re                      
 2              108  Kule K�re                      
 3              262  Kule K�re                      
 4               63  Kule K�re                      
 5             1144  Kule K�re                      
 6              254  Kule K�re                      
 7              173  Kule K�re                      
 8                8  Kule K�re                      
 3               18  Leisure Suit Larry             
 8                9  Leisure Suit Larry             
 4               56  makeITwork as                  
 2                8  Mandatum AS                    
 3               45  Mandatum AS                    
 5               23  Mandatum AS                    
 8               38  Mandatum AS                    
 2               35  Nec                            
 3               42  Nec                            
 5               56  Nec                            
 6               82  Nec                            
 8               27  Nec                            
 0               74  NeverCom                       
 1              150  NeverCom                       
 2              183  NeverCom                       
 3               68  NeverCom                       
 4              264  NeverCom                       
 5              116  NeverCom                       
 6              329  NeverCom                       
 7               61  NeverCom                       
 8               60  NeverCom                       
 1              618  Ole                            
 1                7  Onkel Skrue                    
 4                8  Onkel Skrue                    
 7              668  Onkel Skrue                    
 8              106  Onkel Skrue                    
 0               89  R. Imelig Grimm                
 2               55  R. Imelig Grimm                
 3               78  R. Imelig Grimm                
 4               26  R. Imelig Grimm                
 5              311  R. Imelig Grimm                
 6               26  R. Imelig Grimm                
 7            36938  R. Imelig Grimm                
 8              247  R. Imelig Grimm                
 8               84  Realistforeningens REGI        
 5               59  Room 101                       
 2               36  Skrekk og Gru                  
 2              145  Start                          
 2               23  Statens Helsevesen             
 3               33  Statens Helsevesen             
 5               23  Statens Helsevesen             
 8               85  Statens Helsevesen             
 4              144  Stjel & R�m                    
 7                8  Stjel & R�m                    
 8               98  Stjel & R�m                    
 0               24  Telehor MArketing              
 1               23  Telehor MArketing              
 2               87  Telehor MArketing              
 3               67  Telehor MArketing              
 4               25  Telehor MArketing              
 5              144  Telehor MArketing              
 6               27  Telehor MArketing              
 7               82  Telehor MArketing              
 8               10  Telehor MArketing              
 2                7  THORS                          
 3              127  THORS                          
 4                7  THORS                          
 1               67  Vegard Beider                  
 2               82  Vegard Beider                  
 3               31  Vegard Beider                  
 4             3682  Vegard Beider                  
 5               20  Vegard Beider                  
 8               43  Vegard Beider                  
 0              133  Vegard's datashop              
 1              752  Vegard's datashop              
 2             1068  Vegard's datashop              
 3              995  Vegard's datashop              
 4              263  Vegard's datashop              
 5              769  Vegard's datashop              
 6              983  Vegard's datashop              
 7             1091  Vegard's datashop              
 8             2386  Vegard's datashop              
(216 rows affected)
*** END OF QUERY RESULTS  ***
