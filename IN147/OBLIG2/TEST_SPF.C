#include <stdio.h>
#include <string.h>

extern int spf(char *res, char *format, long a, long b, long c);

int n_feil = 0;


void test(char *f, long a, long b, long c)
{
  char t1[2000], t2[2000];
  int  n1, n2;
  static int n = 0;

  n1 = spf(t1, f, a, b, c);  n2 = sprintf(t2, f, a, b, c);  ++n;
  if (strcmp(t1,t2)==0 && n1==n2) {
    printf("Test #%d OK.\n", n);  return;
  }

  if (strcmp(t1,t2) != 0) {
    printf("Kallet spf(t,\"%s\",%ld,%ld,%ld)\n", f, a, b, c);
    printf("       gir svaret \"%s\",\n", t1);
    printf("    men det burde v�rt \"%s\"!\n", t2);
  }
  if (n1 != n2) {
    printf("Kallet spf(t,\"%s\",%ld,%ld,%ld)\n", f, a, b, c);
    printf("       returnerer %d,\n", n1);
    printf("    men det burde v�rt %d!\n", n2);
  }
  ++n_feil;
}


int main(void)
{
  test("", 0, 0, 0);
  test("En lang tekst uten %%-tegn.", 0, 0, 0);
  test("Ett tegn: `%c'.", 'x', 0, 0);
  test("To tegn: `%c' og `%c'.", 'x', 'y', 0);
  test("Tre tegn: `%c', `%c' og `%c'.", 'x', 'y', 'z');
  test("Lovlige %s er `%%%%', `%cc', `%%d' og `%%s'.",
       (long)"%-spesifikasjoner", '%', 0);
  test("Tre tekster: `%s', `%s' og `%s'.", 
       (long)"abc...���", (long)"alfa -> omega", (long)"");
  test("En �kning p� %d%% er bedre enn en p� %d%%!", 27, 8, 0);
  test("Tallet %d ligger i intervallet %d-%d.", -2230, -10000, -1000);
  test("Tallene er %d, %d og %d.", 0, 1000, 1000000000);
  test("Det %s tallet er %d.", 
       (long)"st�rste positive", 2147483647, 0);
  test("Det %s tallet er %d (-%d).", 
       (long)"st�rste negative", -2147483647, 1);

  printf("%d feil funnet.\n", n_feil);
  return n_feil;
}
