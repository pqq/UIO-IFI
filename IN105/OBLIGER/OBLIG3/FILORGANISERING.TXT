Filorganisering
---------------

    Det er en del av oppgaven � bestemme hvordan data skal lagres
    p� fil. En beskrivelse av dette (med ord og tegninger) skal leveres
    som en del av oppgaven.


-----------------------------------------------------------------------
I klassen bilregister er det laget en prosedyre som skriver til fil,
og en prosedyre som leser fra fil. 
 
i) procedure skriv_til_fil;

   Det f�rste prosedyre gj�r er � �pne filen med navn "bilreg.dta".
   (Filnavnet er bestemt under prosedyren "initialisering" p� slutten
   av programmet.) Og gj�r klart til � skrive linjer til filen p� 55
   tegn. 	
   Vi oppretter en integer med navn teler. Slik kan vi g� igjennom
   registeret en for en bil. Dette blir gjort i for-l�kken, der teller
   man fra 1 til max_antall_biler. I dette tilfellet er
   max_antall_biler lik 1000. 
   S� skjekker vi at n�r pekeren "bilreg(teller)", peker p� et
   bilobjekt. N�r dette er tilfellet skjer f�lgende:
     bilreg(teller) peker p� et bilobjekt. Bilobjektet har en text 
     parameter som er "reg-nr". Og det inneholder en text procedure "hent_eier", og en
     integer procedure "aars_modell".
   F�rst skrives registrerings nummeret ut, p� begynnelsen av linjen.
   Registrerings nummeret er en tekst-streng p� 7 tegn.
   Videre s� plasseres text-objektet sin posisjon p� 9. Dvs p� den
   9'ende plassen p� linjen. Der skrives eieren ut.
   S� settes posisjonen til 50, og �rsmodellen skrives ut.
   Siden �rsmodellen skrives ut p� posisjon 50, s� er det 41 tegn som
   kan brukes til navn. N�r man skriver inn navnet s� kommer det en
   feilmelding hvis navnet er lengre enn denne lengden.
   Tilslutt gj�res en outimage slik at linjen skrives fra bufferet til
   filen.
   N�r hele registeret er gjennomg�tt s� g�r man ut av for-l�kken, og
   man stenger filen.

   Et eksempel p� hvordan en fil kan se ut er:

   AB12345 Margrethe Monsen                         1996
   AB11223 Frode Klevstul                           1995
   AB54321 Ola Presterud                            1996

   Prosedyren:

      PROCEDURE skriv_til_fil;
        BEGIN 
	  INSPECT NEW OutFile(filnavn) DO  ! Lager ny fil med navn filnavn;
	    BEGIN
	      INTEGER teller;

	      Open(Blanks(55));  ! �pner fila, og setter av 55 tegn til skrivebuffer;

	      ! G�r gjennom tabellen, og lagrer informasjon om alle bilene som er registrert;
	      FOR teller := 1 STEP 1 UNTIL max_antall_biler DO  
		BEGIN
		  IF bilreg(teller) =/= NONE THEN
		    BEGIN 		
		      OutText(bilreg(teller).hent_reg_nr);  
		      SetPos(9);OutText(bilreg(teller).hent_eier);
		      SetPos(50);OutInt(bilreg(teller).hent_aarsmodell,4);
		      OutImage;
		    END;
		END;
	      Close;  ! Lukker fila;
	    END;
	END;

   

ii) procedure les_fra_fil;

    Filnavnet vil i dette tilfellet ogs� v�re "bilreg.dta". Dette er
    filen man leser fra. Her har vi brukt for to ting, en integer
    "teller". Denne gj�r den samme nytten som i den foreg�ende
    prosedyren. Grunnen til at vi trenger en text "reg_nr" er at vi m�
    sende med registreringsnummeret som en parameter n�r vi oppretter en
    ny bil. 
    Videre har vi en inimage (leser inn den f�rste linjen fra filen)
    f�r while-l�kken. S� lenge det ikke er slutten av filen s�
    g�r vi inn i l�kken: 
       Telleren vil g� fra 1 og oppover. 
       Registreringsnummeret (reg_nr) settes lik en tekst-streng med
       lengde 7 p� begynnelsen av linjen. 
       Videre opprettes en ny peker til et bilobjekt med reg_nr som
       parameter. Posisjonen til "tekst-leseren" settes s� lik 9 f�r
       eieren leses og sendes til bilobjektet. Det leses inn 41 tegn,
       og vi tar en "strip" slik at de blanke tegnene bak skal
       fjernes.
       Videre leses �rsmodellen, som er en integer. Man trenger ikke
       � sette posisjonen lik 50. "InInt" gj�r at det f�rste heltallet
       leses inn. �rsmodellen sendes til bilobjektet det ogs�.
    Det er en inimage p� slutten av l�kken slik at neste linje i filen
    leses inn. S� lukkes filen og prosedyren avsluttes. Det siste som
    skjer i prosedyren er at antall_biler settes lik teller.


      PROCEDURE les_fra_fil;
        BEGIN
	  INSPECT NEW InFile(filnavn) DO  ! Lese fra fila filnavn;
	    BEGIN
	      INTEGER teller;
	      TEXT reg_nr;

	      ! Sjekker om fila finnes, og �pner med lesebuffer p� 55 tegn;
	      IF Open(Blanks(55)) THEN
		BEGIN
		  InImage;

		  ! Leser dataene fra fila inn i tabellen;
		  WHILE NOT endfile DO 
		    BEGIN
		      teller := teller + 1;
		      reg_nr :- InText(7);
		      bilreg(teller) :- NEW bil(reg_nr);
		      SetPos(9);
		      bilreg(teller).sett_eier(InText(41).Strip);
		      bilreg(teller).sett_aarsmodell(InInt);
		      InImage;
		    END;
		  Close;  ! Lukker fila;
		END;
	      antall_biler := teller;  ! Setter antall biler i tabellen til antall biler som er innlest;
	    END;
	END;






