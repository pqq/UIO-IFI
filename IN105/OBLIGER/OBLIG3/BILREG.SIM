! Oblig 3    Bilregister som bruker SimpleWindow;

! Laget av:  Margrethe Monsen, Frode Klevstul og Ola Presterud;


BEGIN

  ! Leser inn WindowTools, og lager et SimpleWindow vindu;
  EXTERNAL CLASS WindowTools;

  INSPECT NEW SimpleWindow("Bilregister") DO 
  BEGIN 

  ! Global variabel som inneholder dette �ret som tall;
  INTEGER naa;


  ! Klassen "bil" defineres, som inneholder informasjon om hver enkelt bil;
  ! Bil tar ett registreringsnummer som parameter;

  CLASS bil(reg_nr); TEXT reg_nr;
    BEGIN
      INTEGER aarsmodell;   ! Variabel som inneholder �rsmodellen til bilen;
      TEXT eier;            ! Variabel som inneholder eieren av bilen;
      

      ! Viser p� skjermen dataene i objektet;
      PROCEDURE vis_data;
        BEGIN
	  OutText(reg_nr & " " & eier & " ");
	  OutInt(aarsmodell, 0);
	  OutImage;
	END;

      ! Setter eieren;
      PROCEDURE sett_eier(ny_eier); TEXT ny_eier;
        eier :- ny_eier;

      ! Setter �rsmodellen;
      PROCEDURE sett_aarsmodell(aar); INTEGER aar;
        aarsmodell := aar;

      ! Returnerer eieren;
      TEXT PROCEDURE hent_eier;
        hent_eier :- eier;

      ! Returnerer registreringsnummeret;
      TEXT PROCEDURE hent_reg_nr;
        hent_reg_nr :- reg_nr;
  
      ! Returnerer �rsmodellen;
      INTEGER PROCEDURE hent_aarsmodell;
	hent_aarsmodell := aarsmodell; 

    END av bil-klassen;

  
  ! Klassen "register" som inneholder grunnstrukturen for ett register, og;
  ! procedurer for enkel administrasjon av registeret;
  ! Register tar maksimum antall biler og navn p� datafil som parametere;

  CLASS register(max_antall_biler, filnavn); INTEGER max_antall_biler; TEXT filnavn;
    BEGIN    
      REF(bil) ARRAY bilreg(1:max_antall_biler);  ! Tabell som inneholder pekere til max_antall_biler bil objekter;
      INTEGER antall_biler;                       ! Antall biler som er registrert;


      ! Funksjon som s�ker gjennom tabellen etter en bil med registreringsnummeret reg_nr,;
      ! og returnerer tabell-indeksen hvis funnet. Hvis ikke funnet s� 0;
      INTEGER PROCEDURE finn_bil(reg_nr); TEXT reg_nr;
        BEGIN
          INTEGER teller, funnet;

          teller := 1; 
	  funnet := 0;
          WHILE teller <= max_antall_biler AND funnet = 0 DO  ! G�r gjennom tabellen til bilen er funnet;
	    BEGIN                                             ! eller hele tabellen er sjekket;
	      IF bilreg(teller) =/= NONE THEN
		BEGIN
		  IF bilreg(teller).hent_reg_nr = reg_nr THEN
		    funnet := teller;
		END;
	      teller := teller + 1;
	    END;
           
          finn_bil := funnet; ! Indeksen i tabellen til bilen blir returnert;
        END;


      ! Procedure for � vise informasjon om en bestemt bil p� skjermen;
      PROCEDURE Vis_bil;
        BEGIN
	  TEXT reg_nr;
	  INTEGER index;
      
	  reg_nr :- les_reg_nr;  ! Leser inn registreringsnummeret fra brukeren;
	  index := finn_bil(reg_nr);

	  IF index = 0 THEN feil_melding("* Registreringsnummeret er ikke registrert *") ELSE 
	    BEGIN   
	      ! Skriver opplysningene p� skjermen;
	      OutImage;
	      outline("Opplysninger om kj�ret�y: ");
	      bilreg(index).vis_data;
	    END;
	END;
 

      ! Procedure hvor alle dataene om en ny bil blir lest inn fra brukeren, og s� lagt inn i registeret;
      PROCEDURE Ny_bil;
        BEGIN
	  TEXT reg_nr, eier; 
	  INTEGER aar, teller; 

	  ! Sjekker om registeret er fullt;
	  IF antall_biler = max_antall_biler THEN feil_melding("* Registeret er fullt *") ELSE
	    BEGIN
	      reg_nr :- les_reg_nr;  ! Leser inn registreringsnummeret fra brukeren;

	      ! Hvis bilen ikke finnes fra f�r, s� les inn informasjon og lag den;
	      IF finn_bil(reg_nr) <> 0 THEN feil_melding("* Kj�ret�yet er registrert fra f�r *") ELSE
		BEGIN
		  eier :- be_om_text("Skriv inn eier av kj�ret�yet: ").strip; ! Leser inn eier, og sjekker p� lengde;
		  WHILE eier.Length > 40 DO 
		    BEGIN 
		      feil_melding("* Navnet er for langt *");
		      eier :- be_om_text("Skriv inn eier av kj�ret�yet: ").strip;
		    END; 

		  aar := be_om_int("Skriv inn �rsmodellen til kj�ret�yet: ");  ! Leser inn �rsmodell, og sjekker det;
		  WHILE aar < 1900 OR aar > naa DO  ! �rstallet m� v�re mellom 1900 og dette �ret;
		    BEGIN
		      feil_melding("*  du har tastet et ugyldig registrerings�r *");
		      aar := be_om_int("Skriv inn �rsmodellen til kj�ret�yet: ");
		    END;


		  ! Oppretter bilen i registeret, og sender dataene;
		  teller := 1;
		  WHILE bilreg(teller) =/= NONE DO  ! S�ker gjennom tabellen etter f�rste ledige element;
		    teller := teller + 1;

		  bilreg(teller) :- NEW bil(copy(reg_nr));     ! Oppretter ett nytt bil-objekt, og sender med reg_nr;

		  bilreg(teller).sett_eier(copy(eier));        ! Legger inn eier;
		  bilreg(teller).sett_aarsmodell(aar);         ! Legger inn �rsmodell;  
   
		  antall_biler := antall_biler + 1;            ! �ker antall biler med en;

         
		  OutImage;
		  outline("Ny registrering: ");
		  bilreg(teller).vis_data;  ! Skriver ut opplysningene om den nye bilen p� skjermen;
		END;
	    END;     
	END; 


      ! Procedure for � skifte eier p� en eksisterende bil i registeret; 
      PROCEDURE Skift_eier;
        BEGIN
	  INTEGER index;
	  TEXT reg_nr, ny_eier;

	  reg_nr :- les_reg_nr;      ! Leser inn registreringsnummeret fra brukeren;
	  index := finn_bil(reg_nr); ! Finner indeksen til reg_nr;


	  ! Hvis reg_nr finnes, s� be om informasjon og skift eier;
	  IF index = 0 THEN feil_melding("* Registreringsnummeret eksisterer ikke *") ELSE 
	    BEGIN
	      CHARACTER fortsette; 

	      ny_eier :- be_om_text("Skriv inn ny eier: ").strip;  ! Leser inn ny eier;

	      OutImage;   ! Viser opplysninger om bilen;
	      outline("Kj�ret�yet skal skifte eier til " & ny_eier);
	      bilreg(index).vis_data;
	      
	      ! Ber om bekreftelse p� at eier skal skiftes;
	      fortsette :=  be_om_char("Tast 'F' for � fortsette eier skifte: ");  
	      IF NOT (fortsette = 'f' OR fortsette = 'F') THEN feil_melding("Du valgte � avbryte eierskifte") ELSE
		BEGIN   
		  bilreg(index).sett_eier(Copy(ny_eier));  ! Legger inn ny eier;

	    	  OutImage;
		  outline("Bilen " & reg_nr & " har n� skiftet eier til " & ny_eier);
		  OutImage;
		END;
	    END;
	END;


      ! Procedure for � avskilte en bil. Bilen blir da fjernet fra registeret;
      PROCEDURE Avskilt_bil;
        BEGIN 
	  INTEGER index;
	  TEXT reg_nr;

	  reg_nr :- les_reg_nr;      ! Leser inn registreringsnummeret fra brukeren;
	  index := finn_bil(reg_nr); ! Finner indeksen til reg_nr;

	  ! Hvis reg_nr finnes, s� avskilt bilen;
	  IF index = 0 THEN feil_melding("* Registreringsnummeret er ikke registrert *") ELSE 
	    BEGIN
	      CHARACTER fortsette;
	      
	      ! Viser informasjon p� skjermen om reg_nr;
	      OutImage;
	      outline("Kj�ret�yet som skal avskiltes: ");
	      bilreg(index).vis_data;
	      
	      fortsette := be_om_char("Tast 'F' for � fortsette avskiltningen: ");  ! Bekrefter avskiltning;
	      IF NOT (fortsette = 'f' OR fortsette = 'F') THEN feil_melding("Du valgte � avbryte avskiltningen") ELSE
		BEGIN 
		  bilreg(index) :- NONE; ! Fjerner bilen fra registeret;
		  antall_biler := antall_biler - 1;

		  OutImage;
		  outline("Bilen " & reg_nr & " er n� slettet fra registeret");
		  OutImage;
		END;
	    END;
	END;


      ! Procedure for � skrive ut samtlige biler i registeret p� skjermen;
      PROCEDURE Oversikt;
        BEGIN
	  INTEGER teller;

	  OutImage;
	  outline(">>> Kj�ret�yene i registeret <<<");

	  ! L�kke som g�r gjennom hele tabellen, og skriver ut informasjonen der det finnes;
	  FOR teller := 1 STEP 1 UNTIL max_antall_biler DO 
	    BEGIN 
	      IF bilreg(teller) =/= NONE THEN  
		bilreg(teller).vis_data; 
	    END;
	END;
 

      ! Procedure som skriver alle registrerte biler til fil;
      PROCEDURE skriv_til_fil;
        BEGIN 
	  INSPECT NEW OutFile(filnavn) DO  ! Lager ny fil med navn filnavn;
	    BEGIN
	      INTEGER teller;

	      Open(Blanks(55));  ! �pner fila, og setter av 55 tegn til skrivebuffer;

	      ! G�r gjennom tabellen, og lagrer informasjon om alle bilene som er registrert;
	      FOR teller := 1 STEP 1 UNTIL max_antall_biler DO  
		BEGIN
		  IF bilreg(teller) =/= NONE THEN
		    BEGIN 		
		      OutText(bilreg(teller).hent_reg_nr);  
		      SetPos(9);OutText(bilreg(teller).hent_eier);
		      SetPos(50);OutInt(bilreg(teller).hent_aarsmodell,4);
		      OutImage;
		    END;
		END;
	      Close;  ! Lukker fila;
	    END;
	END;


      ! Procedure som leser data fra fil og inn i tabellen;
      PROCEDURE les_fra_fil;
        BEGIN
	  INSPECT NEW InFile(filnavn) DO  ! Lese fra fila filnavn;
	    BEGIN
	      INTEGER teller;
	      TEXT reg_nr;

	      ! Sjekker om fila finnes, og �pner med lesebuffer p� 55 tegn;
	      IF Open(Blanks(55)) THEN
		BEGIN
		  InImage;

		  ! Leser dataene fra fila inn i tabellen;
		  WHILE NOT endfile DO 
		    BEGIN
		      teller := teller + 1;
		      reg_nr :- InText(7);
		      bilreg(teller) :- NEW bil(reg_nr);
		      SetPos(9);
		      bilreg(teller).sett_eier(InText(41).Strip);
		      bilreg(teller).sett_aarsmodell(InInt);
		      InImage;
		    END;
		  Close;  ! Lukker fila;
		END;
	      antall_biler := teller;  ! Setter antall biler i tabellen til antall biler som er innlest;
	    END;
	END;

      END av register-klassen;     


! Klassedeklarasjonene er ferdige, n� starter programmeringen av procedurene som tar;
! for seg brukerdialog og skjermutskrift;


    REF(register) bilregister;  ! Deklarerer peker til klassen "register";


! F�rst noen hjelpeprocedurer for sjekking og innlesing av registreringsnummer og eier;


    ! Funksjon som sjekker at reg_nr er ett lovlig registreringsnummer, dvs.;
    ! totalt 7 tegn, hvor de 2 f�rste er bokstaver og de 5 siste er tall;
    ! Returnerer true hvis riktig, false hvis galt;
    BOOLEAN PROCEDURE sjekk_reg_nr(reg_nr); TEXT reg_nr;
      BEGIN 
	BOOLEAN riktig;
     
	riktig := TRUE;
	IF NOT reg_nr.Length = 7 THEN riktig := FALSE  ! Sjekker lengde;
	ELSE 
	  BEGIN  ! Sjekker bokstaver og tall;
	    IF NOT Letter(reg_nr.GetChar) THEN riktig := FALSE;
	    IF NOT Letter(reg_nr.GetChar) THEN riktig := FALSE;
	    IF NOT Digit(reg_nr.GetChar) THEN riktig := FALSE;
	    IF NOT Digit(reg_nr.GetChar) THEN riktig := FALSE;
	    IF NOT Digit(reg_nr.GetChar) THEN riktig := FALSE;
	    IF NOT Digit(reg_nr.GetChar) THEN riktig := FALSE;
	    IF NOT Digit(reg_nr.GetChar) THEN riktig := FALSE;
	  END;

	sjekk_reg_nr := riktig;  ! Returnerer om reg_nr var riktig eller galt;
      END;
      

    ! Funksjon som leser inn ett registreringsnummer fra brukeren, sjekker om det riktig og returnerer det;
    TEXT PROCEDURE les_reg_nr;
      BEGIN
	TEXT reg_nr; 
       
	! Fortsetter innlesning av registreringsnummer helt til ett riktig nummer blir skrevet inn;
	WHILE NOT sjekk_reg_nr(reg_nr) DO 
	  BEGIN 
	    reg_nr :- Upcase(be_om_text("Skriv inn registrerings nummeret: "));
	    IF NOT sjekk_reg_nr(reg_nr) THEN feil_melding("* Registreringsnummeret er ugyldig *");
	  END;

	les_reg_nr :- reg_nr;  ! Returnerer registreringsnummeret;
      END;


    ! Procedure som viser feilmelding p� skerjmen;
    PROCEDURE feil_melding(melding); TEXT melding;
      BEGIN
	OutImage;
	outline(melding);
	OutImage;
      END;


    ! Procedure som skriver hjelp ut p� skjermen;
    PROCEDURE hjelp;
      BEGIN
	OutImage;
	OutImage;
	outline("         Dette programmet kan utf�re f�lgende ordre: ");
	outline("-----------------------------------------------------------------");
	outline("Ny bil:      Legger inn data om en ny bil");
	outline("Skift eier:  Registrerer ny eier for en registrert bil");
	outline("Avskilt bil: Sletter alle data om en bil i registeret");
	outline("Vis bil:     Skriver ut data til bil med gitt registreringsnummer");
	outline("Oversikt:    Skriver oversikt over alle bilene i registeret");
	outline("Hjelp:       Skriver denne listen med lovlige ordre");
	outline("Avslutt:     Avslutter");
	OutImage;
      END;

 

! Resten av procedurene har med oppstart og avsluttning av programmet, samt hovedl�kka; 

   
   ! Denne proceduren klargj�r programmet for kj�ring;
   PROCEDURE initialiser;
     BEGIN 
       ! Register-objekt med plass til 1000 biler lages;
       bilregister :- NEW register(1000, "bilreg.dta");

       ! Informasjon leses fra fil;
       bilregister.les_fra_fil;

       ! Knappene lages;
       makebutton("Ny bil");
       makebutton("Skift eier");
       makebutton("Avskilt bil");
       makebutton("Vis bil");
       makebutton("Oversikt");
       makebutton("Hjelp");
       makebutton("Avslutt");

       ! Skriver ut hjelp p� skjermen;
       hjelp;  
     END;

 
   ! Her er hovedl�kka i programmet, som venter p� at brukeren skal trykke p� en knapp,;
   ! for s� � utf�re det brukeren ber om. Avslutter n�r brukeren trykker p� Avslutt knappen;
   PROCEDURE hovedloekke;
     BEGIN 
       TEXT ordre;

       ordre:-ButtonChoice;  ! Leser inn f�rste ordre;

       WHILE NOT ordre="Avslutt" DO  ! Holder p� helt til Avslutt blir trykket p�;
	 BEGIN
	   IF ordre="Ny bil"      THEN bilregister.Ny_bil      ELSE    ! Legger inn ny bil;
	   IF ordre="Skift eier"  THEN bilregister.Skift_eier  ELSE    ! Skifter eier p� bil;
	   IF ordre="Avskilt bil" THEN bilregister.Avskilt_bil ELSE    ! Avskilte bil;
	   IF ordre="Vis bil"     THEN bilregister.Vis_bil     ELSE    ! Viser bestemt bil;
	   IF ordre="Oversikt"    THEN bilregister.Oversikt    ELSE    ! Viser alle biler;
	   IF ordre="Hjelp"       THEN Hjelp;                          ! Viser hjelp; 
	  
	   ordre:-ButtonChoice;                                        ! Venter p� neste ordre;
	 END;
     END;


   ! Denne proceduren gj�r alt klart for avsluttning av programmet;
   PROCEDURE avslutt;
     BEGIN
       ! Dataene om bilene blir lagret til fil;
       bilregister.skriv_til_fil;
     END;


! Hovedprogrammet kommer her;
  
  naa := 1996;   ! �rstallet settes;

  initialiser;   ! Initialiserer programmet;
  hovedloekke;   ! Starter hovedl�kka;
  avslutt;       ! Avslutter programmet;


  END of SimpleWindow;

END ferdig med programmet;








