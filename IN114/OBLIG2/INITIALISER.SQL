/* Har hatt problemer med � teste sp�rringer, triggere og innleggelse  	*/
/* pga at det er noen triggere jeg ikke klarer � slette. Jeg har sendt 	*/
/* mail til sybase@ifi og drift@ifi om det problemet men jeg har ikke  	*/
/* f�tt noen svar enda. Men her er i hvertfall litt:			*/

delete Timelistelinje
go
delete PROSJEKT_DELTAGELSE
go
delete GODKJENT
go
delete DELTID
go
delete FULLTID
go 
delete TIME_BASIS
go
delete PROSJEKT
go
delete FAST_ANSATT
go
delete Timeliste
go
delete ANSATT
go
delete LONNSTRINN
go 



insert into LONNSTRINN values (1,10000,150,270)
go
insert into LONNSTRINN values (2,15000,200,320)
go

insert into ANSATT values (1,1)
go
insert into ANSATT values (2,2)
go

insert into TIME_BASIS values(1)
go

insert into FAST_ANSATT values (2)
go

insert into DELTID values (2,25)
go

insert into Timeliste values (1,'a',1)
go
insert into Timeliste values (2,'a',2)


insert into PROSJEKT values (1,10,8,2)
go
insert into PROSJEKT values (2,10,8,2)
go

insert into PROSJEKT_DELTAGELSE values (1,1)
go

insert into GODKJENT values ('J')
go



insert into GODKJENT values ('N')
go

insert into Timelistelinje values ('1.9.1997 08:00', '1.9.1997 10:00', 00, 1, 1, 'N')
go
insert into Timelistelinje values ('2.9.1997 08:00', '2.9.1997 16:00', 30, 1, 1, 'N')
go
insert into Timelistelinje values ('3.9.1997 08:00', '3.9.1997 16:00', 30, 1, 1, 'N')
go
insert into Timelistelinje values ('4.9.1997 08:00', '4.9.1997 17:00', 45, 1, 1, 'N')
go



insert into FAST_ANSATT values (1)
go
/* ==================================================== */
/* Msg 20005, Level 16, State 1:			*/		
/* Procedure 'enten_ellerI', Line 18:			*/
/* En fast ansatt kan ikke v�re ansatt p� timebasis	*/
/* ==================================================== */


insert into TIME_BASIS values (2)
go
/* ==================================================== */
/* Msg 20005, Level 16, State 1:			*/
/* Procedure 'enten_ellerII', Line 17:			*/
/* En deltidsansatt kan ikke ogs� v�re fast ansatt	*/
/* ==================================================== */



insert into Timelistelinje values ('4.9.1997 10:00', '4.9.1997 17:00', 45, 2, 2, 'N')
go
/* ==================================================== */
/* Msg 20005, Level 16, State 1:			*/
/* Procedure 'ans_paa_prosjekt', Line 18:		*/
/* Du kan ikke legge inn timer pae et prosjekt du ikke 	*/
/* jobber pe						*/
/* ==================================================== */



select * from tll_med_varighet
go
/* ============================================================================== */
/* starttid                   sluttid                    pause       varighet  	  */ 
/*         timeliste#  PROSJEKT    GODKJENT 					  */
/* -------------------------- -------------------------- ----------- -----------  */
/*        ----------- ----------- -------- 					  */
/*        Jan  9 1997  8:00AM        Jan  9 1997 10:00AM           0         120  */
/*                   1           1 N        					  */
/*        Feb  9 1997  8:00AM        Feb  9 1997  4:00PM          30         450  */
/*                   1           1 N          					  */
/*        Mar  9 1997  8:00AM        Mar  9 1997  4:00PM          30         450  */
/*                   1           1 N        					  */
/*        Apr  9 1997  8:00AM        Apr  9 1997  5:00PM          45         495  */
/*                   1           1 N        				  	  */
/* 										  */
/*(4 rows affected)								  */
/* ============================================================================== */



select FOR_ANSATT 
from Timeliste
where timeliste# = 1
go
/* ================ */
/*  FOR_ANSATT 	    */ 
/*  -----------     */
/*            1     */
/* 		    */
/* (1 row affected) */
/* ================ */

