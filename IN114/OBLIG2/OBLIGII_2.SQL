/* ============================================================ */
/*               OBLIG II i IN114 for gruppe V                  */
/*                                                              */
/*   Laget den:      10/19/97                                   */
/*   Erik Grobstok (erikg) og Frode Klevstul (frodekl)          */
/* ============================================================ */





/* ============================================================ */
/*  Sletter databasen  						*/
/* ============================================================ */

/* ============================================================ */
/* SP�RSM�L:                                                    */
/* Finnes det ingen kommando for � slette hele base???          */
/* ============================================================ */


/*drop trigger prosjektleder_varsling*/
/*go*/
/*drop trigger ansatt_varsling*/
/*go*/
/*drop trigger ans_paa_prosjekt*/
/*go*/

drop view tll_med_varighet
go
drop trigger hindre_timeoverlapping_
go

/*drop trigger prosjektleder_i_prosj_delt*/
/*go*/

drop trigger tl_levert_er_ok 
go
drop trigger tll_ikke_levert
go
drop table Timelistelinje
go
drop table PROSJEKT_DELTAGELSE
go
drop table GODKJENT
go
drop table DELTID
go
drop table FULLTID
go 
drop table TIME_BASIS
go
drop table PROSJEKT
go
drop table FAST_ANSATT
go
drop table Timeliste
go
drop table ANSATT
go
drop table LONNSTRINN
go 


/* ============================================================ */
/*   Table: Timeliste                                           */
/* ============================================================ */

create table Timeliste
  (
   timeliste#  	int NOT 	NULL,
   status      	char (1) 	NOT NULL,
   FOR_ANSATT 	int		NOT NULL 
  )
go

alter table Timeliste
  add constraint pkey_timeliste
  primary key (timeliste#)
go

alter table Timeliste
  add constraint timeliste_status_gyldig
  check (status in ('a', /* aktiv*/
		    'l', /* levert */		  
		    'u'))/* utbetalt */
go

/* ============================================================ */
/*   Table: Timelistelinje                                      */
/* ============================================================ */

create table Timelistelinje
  (
   /* startdato */
   aar_start    int NOT NULL,
   mnd_start    int NOT NULL,
   dag_start    int NOT NULL,
   /* starttid */
   time_start   int NOT NULL,
   minutt_start int NOT NULL,
   /* sluttid */
   aar_slutt    int NOT NULL,
   mnd_slutt    int NOT NULL,
   dag_slutt    int NOT NULL,
   time_slutt   int NULL,
   minutt_slutt int NULL,
   /* pause i minutter */
   pause        int NULL,
   /* hvilken timeliste st�r dette p�? */
   timeliste#   int NOT NULL references Timeliste,
   PROSJEKT 	int NULL,
   GODKJENT	char(1)	null 
  )
go

/*
  Prim�rn�kkel: startdato
  */

alter table Timelistelinje
  add constraint pkey_timelistelinje
  primary key (aar_start, mnd_start, dag_start, time_start, minutt_start, timeliste#)
go

/*
  Domenerestriksjoner
  */

alter table Timelistelinje
  add constraint tll_mnd_gyldig
  check (mnd_start between 1 and 12)
go

alter table Timelistelinje
  add constraint tll_dag_gyldig
  check (dag_start between 1 and 31)
go

alter table Timelistelinje
  add constraint tll_time_start_gyldig
  check (time_start between 0 and 23)
go

alter table Timelistelinje
  add constraint tll_minutt_start_gyldig
  check (minutt_start between 0 and 59)
go

alter table Timelistelinje
  add constraint tll_time_slutt_gyldig
  check (time_slutt between 0 and 23)
go

alter table Timelistelinje
  add constraint tll_minutt_slutt_gyldig
  check (minutt_slutt between 0 and 59)
go

/* ============================================================ */
/*   Table: GODKJENT                                            */
/* ============================================================ */
create table GODKJENT
(
    LOVELIGE_VERDIER  char(1)                not null,
    constraint PK_GODKJENT primary key (LOVELIGE_VERDIER)
)
go

/* ============================================================ */
/*   Table: PROSJEKT                                            */
/* ============================================================ */
create table PROSJEKT
(
    PROSJEKT_KODE     int                    not null,
    BUDSJETT_RAMME    int                    null    ,
    SKAL_VARSLES      int                    null    ,
    PROSJEKT_LEDER    int                    null    ,
    constraint PK_PROSJEKT primary key (PROSJEKT_KODE)
)
go

/* ============================================================ */
/*   Table: LONNSTRINN                                          */
/* ============================================================ */
create table LONNSTRINN
(
    TRINN_NR          int                    not null,
    MND_LONN          int                    null    ,
    TIME_LONN         int                    null    ,
    OVERTID           int                    null    ,
    constraint PK_LONNSTRINN primary key (TRINN_NR)
)
go

/* ============================================================ */
/*   Table: ANSATT                                              */
/* ============================================================ */
create table ANSATT
(
    ANSATT_NR         int                    not null,
    TRINN_NR          int                    null    ,
    constraint PK_ANSATT primary key (ANSATT_NR)
)
go

/* ============================================================ */
/*   Table: FAST_ANSATT                                         */
/* ============================================================ */
create table FAST_ANSATT
(
    ANSATT_NR         int                    not null,
    constraint PK_FAST_ANSATT primary key (ANSATT_NR)
)
go

/* ============================================================ */
/*   Table: DELTID                                              */
/* ============================================================ */
create table DELTID
(
    ANSATT_NR         int                    not null,
    SKAL_JOBBE        int                    null    ,
    constraint PK_DELTID primary key (ANSATT_NR)
)
go

/* ============================================================ */
/*   Table: FULLTID                                             */
/* ============================================================ */
create table FULLTID
(
    ANSATT_NR         int                    not null,
    constraint PK_FULLTID primary key (ANSATT_NR)
)
go

/* ============================================================ */
/*   Table: TIME_BASIS                                          */
/* ============================================================ */
create table TIME_BASIS
(
    ANSATT_NR         int                    not null,
    constraint PK_TIME_BASIS primary key (ANSATT_NR)
)
go

/* ============================================================ */
/*   Table: PROSJEKT_DELTAGELSE                                 */
/* ============================================================ */
create table PROSJEKT_DELTAGELSE
(
    PROSJEKT_KODE     int                    not null,
    ANSATT_NR         int                    not null,
    constraint PK_PROSJEKT_DELTAGELSE primary key (PROSJEKT_KODE, ANSATT_NR)
)
go

/* =========================================================== */
/*               KNYTTER TIL FREMMEDN�KLER                     */
/* =========================================================== */

alter table Timeliste
    add constraint FK_Timeliste_REF_ANSATT foreign key (FOR_ANSATT)
	references ANSATT (ANSATT_NR)
go

alter table Timelistelinje
    add constraint FK_Timelistelinje_REF_PROSJEKT foreign key (PROSJEKT)
	references PROSJEKT(PROSJEKT_KODE)
go

alter table Timelistelinje
    add constraint FK_Timelistelinje_REF_GODKJENT foreign key (GODKJENT)
	references GODKJENT (LOVELIGE_VERDIER)
go

alter table ANSATT
    add constraint FK_ANSATT_REF_LONNSTRINN foreign key  (TRINN_NR)
       references LONNSTRINN (TRINN_NR)
go

alter table FAST_ANSATT
    add constraint FK_FAST_ANS_REF_ANSATT foreign key  (ANSATT_NR)
       references ANSATT (ANSATT_NR)
go

alter table DELTID
    add constraint FK_DELTID_REF_FAST_ANSATT foreign key  (ANSATT_NR)
       references FAST_ANSATT (ANSATT_NR)
go

alter table FULLTID
    add constraint FK_FULLTID_REF_FAST_ANSATT foreign key  (ANSATT_NR)
       references FAST_ANSATT (ANSATT_NR)
go

alter table TIME_BASIS
    add constraint FK_TIME_BAS_REF_ANSATT foreign key  (ANSATT_NR)
       references ANSATT (ANSATT_NR)
go

alter table PROSJEKT
    add constraint FK_PROSJEKT_REF_FAST_ANSATT foreign key (PROSJEKT_LEDER)
	references FAST_ANSATT (ANSATT_NR)

go

alter table PROSJEKT_DELTAGELSE
    add constraint FK_PROSJEKT_REF_ANSATT foreign key  (ANSATT_NR)
       references ANSATT (ANSATT_NR)
go

alter table PROSJEKT_DELTAGELSE
    add constraint FK_PROSJEKT_REF_PROSJEKT foreign key  (PROSJEKT_KODE)
       references PROSJEKT (PROSJEKT_KODE)
go




/* ========================================================= */
/*              LAGER NY VIEW                                */
/* ========================================================= */

/*
  View tll_med_varighet
  */


create view tll_med_varighet as
(
  select aar_start, mnd_start, dag_start,
         time_start, minutt_start,
         time_slutt, minutt_slutt,
    pause,
  /* varighet er i minutter */
    varighet = (((time_slutt*60)+minutt_slutt-(time_start*60)
		 -minutt_start-pause))
               + (sign ((((time_slutt*60)+minutt_slutt-(time_start*60)
			  -minutt_start-pause - 0.5)))*(-12)*60)
               + 12*60 ,
    timeliste#, PROSJEKT, GODKJENT
  from Timelistelinje )
go


/* ========================================================= */
/*              LAGER TRIGGERE                               */
/* ========================================================= */

/* --------------------------------------------------------- */
/* === 		PROSJEKTLEDER VARSLING			 === */
/* --------------------------------------------------------- */
create trigger prosjektleder_varsling
on Timelistelinje
for update
as begin

declare @totalt_godkjente_timer integer
declare @nye_godkj_timer integer
declare @varsling	 integer

	select @totalt_godkjente_timer = tmv.varighet/60
	from inserted i, tll_med_varighet tmv
	where i.PROSJEKT = tmv.PROSJEKT and
	tmv.GODKJENT = 'J'

	select @varsling = sum (P.BUDSJETT_RAMME - P.SKAL_VARSLES)
	from PROSJEKT P, inserted i
	where i.PROSJEKT = P.PROSJEKT_KODE

	if (@totalt_godkjente_timer >= @varsling)
		begin
			raiserror 20004 "Huups, f�lg med p� budjettrammen!"
		end
end

go

/* --------------------------------------------------------- */
/* === 		ANSATT VARSLING				 === */
/* --------------------------------------------------------- */
create trigger ansatt_varsling
on Timelistelinje
for insert
as begin

declare @innlagt_av_ansatt 	integer
declare @totalt_godkjente_timer integer
declare @varsling	 	integer
declare @budsjettramme	 	integer
declare @balanse		integer
declare @utskift		varchar(10)

	select @totalt_godkjente_timer = tmv.varighet/60
	from inserted i, tll_med_varighet tmv
	where i.PROSJEKT = tmv.PROSJEKT and
	tmv.GODKJENT = 'J'

	select @varsling = sum (P.BUDSJETT_RAMME - P.SKAL_VARSLES)
	from PROSJEKT P, inserted i
	where i.PROSJEKT = P.PROSJEKT_KODE

	select @innlagt_av_ansatt = tmv.varighet/60
	from inserted i, tll_med_varighet tmv
	where tmv.aar_start = i.aar_start and
	      tmv.mnd_start = i.mnd_start and
	      tmv.dag_start = i.dag_start and
	      tmv.time_start = i.time_start and
	      tmv.minutt_start = i.minutt_start and
	      tmv.timeliste# = i.timeliste#

	select @budsjettramme = BUDSJETT_RAMME
	from PROSJEKT P, inserted i
	where P.PROSJEKT_KODE = i.PROSJEKT
 
	select @balanse = sum (P.BUDSJETT_RAMME - @totalt_godkjente_timer )
	from PROSJEKT P, inserted i
	where i.PROSJEKT = P.PROSJEKT_KODE


select @utskift = convert(varchar(10), @balanse)

if (@totalt_godkjente_timer+@innlagt_av_ansatt)>@varsling
		begin
			raiserror 20005 "Huuups, f�lg med p� budjettrammen!
                        Budsjettbalansen er: @utskrift"
		end
end

go

/* --------------------------------------------------------- */
/* === 	ANSATT P� PROSJEKT (som han skriver timer p�)	 === */
/* --------------------------------------------------------- */
create trigger ans_paa_prosjekt
on Timelistelinje 
for 
insert 
as begin

declare  @verdi integer

	select @verdi= count (PD.PROSJEKT_KODE)
	from PROSJEKT_DELTAGELSE PD, inserted i, Timeliste T, ANSATT A
	where 	i.timeliste# = T.timeliste# and
 		T.FOR_ANSATT = A.ANSATT_NR and
		PD.ANSATT_NR = A.ANSATT_NR


if @verdi=0
	begin
		raiserror 20005 "Du kan ikke legge inn timer pa� et prosjekt du ikke jobber p�"
		rollback transaction
	end
end

go

/* --------------------------------------------------------- */
/* === ENTEN TIMEBASIS ELLER FASTANSATT                      */
/* --------------------------------------------------------- */
create trigger enten_ellerI
on FAST_ANSATT
for insert, update
as begin

declare @verdi 	integer

select @verdi = count (t.ANSATT_NR)
from TIME_BASIS T, inserted i
where T.ANSATT_NR = i.ANSATT_NR

if @verdi>0
	begin
		raiserror 20005 "En fast ansatt kan ikke v�re ansatt p� timebasis"
		rollback transaction
	end
end
go

/* --------------------------------------------------------- */
/* === ENTEN TIMEBASIS ELLER FASTANSATT II                   */
/* --------------------------------------------------------- */
create trigger enten_ellerII
on TIME_BASIS
for insert, update
as begin
declare @verdi 	integer

select @verdi = count (F.ANSATT_NR)
from FAST_ANSATT F, inserted i
where F.ANSATT_NR = i.ANSATT_NR

if @verdi>0
	begin
		raiserror 20005 "En deltidsansatt kan ikke ogs� v�re fast ansatt"
		rollback transaction
	end
end
go


/* --------------------------------------------------------- */
/* === PROSJEKTLEDER FINNES I PROSJEKT_DELTAGELSE TABELLEN   */
/* --------------------------------------------------------- */

/*create trigger prosjektleder_i_prosj_delt*/
/*on PROSJEKT*/
/*for insert */
/*as begin*/

/*declare	@verdi integer*/

/*select @verdi = count (PD.ANSATT_NR)*/
/*from PROSJEKT_DELTAGELSE PD, inserted i*/
/*where i.PROSJEKT_LEDER = PD.ANSATT_NR*/

/*if @verdi=0*/
/*	begin */
/*		raiserror 20005 "En som ikke jobber p� et prosjekt kan ikke v�re prosjektleder"*/
/*	end*/
/*end*/
/*go*/

/* --------------------------------------------------------- */
/* ===		TIMEOVERLAPP HINDRING			 === */
/* --------------------------------------------------------- */
create trigger hindre_timeoverlapping_
on Timelistelinje
for insert, update
as begin


declare @start		int
declare @slutt		int
declare @innlagt_start  int
declare @innlagt_slutt	int

	select @start = sum(	TL.aar_start	*100000000+ 
				TL.mnd_start	*1000000+
				TL.dag_start	*10000+
				TL.time_start	*100+
				TL.minutt_start *1)
	from Timelistelinje TL, inserted i
	where TL.timeliste# IN (
		select T.timeliste# 
		from Timeliste T
		where FOR_ANSATT IN (	
			select FOR_ANSATT
			from Timeliste T, inserted i
			where i.timeliste# = T.timeliste#))
	and not (i.timeliste#  = TL.timeliste# and
		 i.aar_start   = TL.aar_start  and
		 i.dag_start   = TL.dag_start  and
		 i.time_start  = TL.time_start and
		 i.minutt_start= TL.minutt_start) 


	select @slutt = sum(	TL.aar_slutt	*100000000+
				TL.mnd_slutt	*1000000+
				TL.dag_slutt	*10000+
				TL.time_slutt	*100+
				TL.minutt_slutt *1)
	from Timelistelinje TL, inserted i
	where TL.timeliste# IN (
		select T.timeliste# 
		from Timeliste T
		where FOR_ANSATT IN (	
			select FOR_ANSATT
			from Timeliste T, inserted i
			where i.timeliste# = T.timeliste#))
	and not (i.timeliste#  = TL.timeliste# and
		 i.aar_start   = TL.aar_start  and
		 i.dag_start   = TL.dag_start  and
		 i.time_start  = TL.time_start and
		 i.minutt_start= TL.minutt_start)


	select @innlagt_start = sum(	i.aar_start	*100000000+ 
					i.mnd_start	*1000000+
					i.dag_start	*10000+
					i.time_start	*100+
					i.minutt_start 	*1)
	from inserted i

	
	select @innlagt_slutt = sum(	i.aar_slutt	*100000000+
					i.mnd_slutt	*1000000+
					i.dag_slutt	*10000+
					i.time_slutt	*100+
					i.minutt_slutt 	*1)
	from inserted i
	


if @innlagt_start between @start and @slutt  
	begin
		raiserror 20005 "Timeoverlapp er ikke lov"
		rollback transaction
	end

if @innlagt_slutt between @start and @slutt
	begin
		raiserror 20005 "Timeoverlapp er ikke lov"
		rollback transaction
	end

if @start between @innlagt_start and @innlagt_slutt
	begin
		raiserror 20005 "Timeoverlapp er ikke lov"
		rollback transaction
	end

if @slutt between @innlagt_start and @innlagt_slutt
	begin
		raiserror 20005 "Timeoverlapp er ikke lov"
		rollback transaction
	end

end
go




/* ============================================================ */
/*   Triggere fra oblig1                                        */
/* ============================================================ */


/*
  Triggere vi trenger :
  */

/*
  Ved oppdatering av timeliste -> levert skal det ikke forekomme
  NULL-verdier for n�r en time ble avsluttet
  */

create trigger tl_levert_er_ok 
on timeliste
for update
as
if update (timeliste#)
begin
    rollback transaction
    print "Du kan ikke forandre et timelistenummer i Timeliste."
end
if update (status)
begin
    if (select count(*) from timelistelinje, inserted
	where timelistelinje.timeliste# = inserted.timeliste#
	and inserted.status = 'l'
	and ( timelistelinje.time_slutt = NULL
		or timelistelinje.minutt_slutt = NULL )) > 0
    begin
	rollback transaction
	print "Det er ikke mulig � levere timelister med uavsluttede timelinjer."
    end
    if (select count(*) from deleted, inserted
	where deleted.timeliste# = inserted.timeliste#
	and deleted.status = 'a'
	and inserted.status = 'u') > 0
    begin
	rollback transaction
	print "Du kan ikke oppdatere fra a(ktiv) til u(tbetalt) uten"
	print "� g� gjennom l(evert)."
    end
end

go

/*
  Ved innsetting og oppdatering i timelistelinje m� tilsvarende
  timeliste verken v�re levert eller utbetalt
  */


create trigger tll_ikke_levert
on timelistelinje
for update, insert
as
if (select count(*) from inserted, timeliste
    where timeliste.timeliste# = inserted.timeliste#
    and timeliste.status != 'a') > 0
begin
    rollback transaction
    print "Du kan ikke legge inn data i en timeliste som ikke er aktiv."
end

go

