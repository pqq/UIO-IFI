/* Sletter gamle verdier i tabellene */

DELETE FROM	Timelistelinje
go
DELETE FROM	Timeliste
go


/* Begynner aa legge inn data */

INSERT INTO	Timeliste
		Values(1,'a')
go
INSERT INTO	Timeliste
		Values(2,'a')
go
INSERT INTO	Timeliste
		Values(3,'u')
go
INSERT INTO	Timeliste
		Values(4,'l')
go
INSERT INTO	Timeliste
		Values(5,'l')
go
INSERT INTO	Timeliste
		Values(6,'a')
go
INSERT INTO	Timeliste
		Values(7,'a')
go
INSERT INTO	Timeliste
		Values(8,'a')
go
INSERT INTO	Timelistelinje 
		Values(97,07,05,08,00,16,00,30,1)
go
INSERT INTO	Timelistelinje 
		Values(97,07,06,08,00,16,00,30,1)
go
INSERT INTO	Timelistelinje 
		Values(97,07,07,08,00,16,00,30,1)
go
INSERT INTO	Timelistelinje 
		Values(97,07,08,08,00,16,00,30,1)
go
INSERT INTO	Timelistelinje 
		Values(97,07,31,20,00,02,00,30,1)
go
INSERT INTO	Timelistelinje 
		Values(97,08,01,12,00,17,00,30,1)
go
INSERT INTO	Timelistelinje 
		Values(96,07,05,08,00,16,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(96,07,06,08,00,16,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(96,07,07,08,00,16,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(96,07,08,08,00,16,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(96,07,31,20,00,02,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(96,08,01,12,00,17,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(96,09,05,08,00,16,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(96,09,06,08,00,16,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(96,09,07,08,00,16,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(96,09,08,08,00,16,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(97,09,31,20,00,02,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(97,10,01,12,00,17,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(97,10,05,08,00,16,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(97,10,06,08,00,16,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(97,10,07,08,00,16,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(97,10,08,08,00,16,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(97,10,31,20,00,02,00,30,8)
go
INSERT INTO	Timelistelinje 
		Values(97,11,01,12,00,17,00,30,8)
go
go

/* to 'go'er pga at det var noedvendig under kjoering */


