import java.lang.*;
import java.util.ArrayList;
import java.util.List;

class Block{
	
	protected Block 	mNext;
	protected int 		mBlockLevel;
	protected Block 	mPrevious;
	protected String	mBlockName;
	protected int		mProcedureNumber;
	protected List 		variables = new ArrayList(); 


	// Constructor
	public Block(String blockName, int procNo){
		mBlockLevel = 0;								// alle blokker f�r i utgangspunktet niv� 0, men dette forandres i "addBlock()" for alle untatt f�rste blokk
		mBlockName = blockName;
		mProcedureNumber = procNo;
	}


	public void addBlock(Block newBlock){
		this.setNext(newBlock);							// denne sin neste blir den nye blokken
		newBlock.setPrevious(this);						// den nye blokken sin forrige blir denne
		mNext.setLevel(this.getLevel() + 1);			// blokkniv�et til neste blokk settes lik "this" + 1
	}

	public void setNext(Block nextBlock){
		mNext = nextBlock;
	}

	public Block getNext(){
		return mNext;
	}

	public void setPrevious(Block previousBlock){
		mPrevious = previousBlock;
	}

	public Block getPrevious(){
		return mPrevious;
	}
	
	public int getLevel(){
		return mBlockLevel;
	}
	
	public void setLevel(int pLevel){
		mBlockLevel = pLevel;
	}

	public void addVariable(String varName){
		variables.add(varName);
	}


	public boolean containsVariable(String varName){
		if (variables.contains(varName)){
			return true;
		} else {
			return false;
		}
	}

	public int getVariableIndex(String varName){
		return variables.indexOf(varName);
	}

	public String getName(){
		return mBlockName;
	}

	public int getNumberVariables(){
		return variables.size();
	}

	public int getProcNo(){
		return mProcedureNumber;
	}

	public void printVariables(){
		int i;
		for (i = 0; i < variables.size(); i++){
			System.out.println("\nFK::: variable name "+ variables.get(i) );
		}
	}
	

	public void printVariable(int index){
		System.out.println("\nFK::: variable '" + index + "': " + variables.get(index) );
	}

}

