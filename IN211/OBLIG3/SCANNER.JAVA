/*
Class Scanner implements a scanner suitable for parsing Logla programs. 
It is implemented as an extension of the standard class StreamTokenizer
and is initialized as

    Scanner s = new Scanner(new FileReader(new File(fileName)), fileName)

in which case it will read tokens from the file named `fileName'.

Each call on `s.readNext()' will place the next token in `s.curToken'.
(The class Token is described later in this file.) Five other methods 
are available:

    s.warning("...") will print an error message stating the correct 
        file name and line number.
    s.error("...") will print an error message (like s.warning) and
        then terminate the program.
    s.check("..."), s.image(), and s.is("...") are just interfaces to
        methods by the same name in Token and subclasses; see below.

Known bugs:

  * Decimal numbers (like 3.14 or 7.98) are accepted as integers (3 and
    7) but a warning is given.
*/

import java.io.*;

class Scanner extends StreamTokenizer {
  String curFileName;
  Token  curToken;

  Scanner(FileReader input, String fileName) {
    super(input);  curFileName = fileName;

    commentChar('%');
    ordinaryChar('-');  ordinaryChar('/');
    ordinaryChar('"');  ordinaryChar('\'');

    readNext();
  }

  void readNext() {
    char c;

    try {
      nextToken();
    } catch (IOException e) { curToken = new EofToken(); return; }

    switch (ttype) {
    case TT_EOF:    curToken = new EofToken();  break;
    case TT_NUMBER: if (nval != (double)(int)nval) 
                      warning("Number " + nval + " is not an integer.");
                    curToken = new NumToken((int)nval);  break;
    case TT_WORD:   if (sval.equals("assign") ||
			sval.equals("begin") ||
			sval.equals("call") ||
			sval.equals("end") ||
			sval.equals("fi") ||
			sval.equals("if") ||
			sval.equals("procedure") ||
			sval.equals("then") ||
			sval.equals("var"))
                      curToken = new ResWordToken(sval);
                    else curToken = new IdToken(sval);
                    break;
    default:        c = (char)ttype;
                    if (c == ';' || c == ',' || c == '>' || c == '+' ||
			c == '-' || c == '*' || c == '/' || c == '?' ||
			c == '!')
		      curToken = new SyToken(String.valueOf(c));
		    else error("Illegal symbol: '" + c + "'");
    }
  }

  void check(String t) { curToken.check(this, t); }
  String image()       { return curToken.image(); }
  int num() { return curToken.num(); } // FK
  boolean is(String t) { return curToken.is(t);   }

  void warning(String message) {
    System.err.println("Error in "+curFileName+" on line "+lineno()+":");
    System.err.println("  " + message);
  }

  void error(String message) {
    warning(message);  System.exit(1);
  }

}


/*
Class Token and subclasses represent tokens read by the scanner:

    EofToken: Found when all tokens in the file have been read.

    NumToken: Integer constants, like 0 or 123.

    IdToken:  Identifiers, like `X' or `myName'; no reserved words.
              Note that `A' and `a' are two different identifiers.

    ResWordToken: Reserved words, like `procedure' or `if'. They
              are all lowercase.

    SyToken:  Legal symbols, like `+' or `>'.

The static variable `testing' can be set:

    Token.testing = true;

in which case debugging information is printed.

There are three utility methods in Token objects:

    image() returns a String with a suitable representation of the token;
        this is useful for debugging and error messages.
    is("...") checks whether the token is what it is supposed to be (as
        specified by the parameter). ResWordToken and SyToken must match 
	exactly; all NumToken objects accept "NUM", all IdToken objects
	answer to "ID", and all EofToken objects fit "EOF".
    check("...") also checks the token, and if not correct (as described
        for `is'), the program terminates after giving an error message.
*/

abstract class Token {
  static boolean testing = false;

  protected void testS(String message) {
    if (testing) System.out.println("Scanner> " + message);
  }

  void    check(Scanner s, String t) { s.error("Syntax error!"); }
  String  image()                    { return ""; }
  int num() { return 0; } // FK
  boolean is(String t)               { return false; }
  
}

class EofToken extends Token {
  EofToken() { 
    testS("Read EofToken"); 
  }

  String  image()      { return "E-o-F"; }
  boolean is(String t) { return t.equals("EOF"); }
}

class NumToken extends Token {
  int val;

  NumToken(int n) { 
    val = n;  testS("Read NumToken " + n); 
  }

  void check(Scanner s, String t) {
    if (! is(t)) s.error("A number expected.");
  }
  String  image()      { return Integer.toString(val); }
  int num() { return val; } // FK
  boolean is(String t) { return t.equals("NUM"); }

}

class IdToken extends Token {
  String id;

  IdToken(String t) { this(t, false); }

  IdToken(String t, boolean is_a_subclass) {
    id = t; 
    if (! is_a_subclass) testS("Read IdToken " + t); 
  }

  void check(Scanner s, String t) {
    if (! is(t)) s.error("A name expected.");
  }
  String  image()      { return id; }
  boolean is(String t) { return t.equals("ID"); }
}

class ResWordToken extends IdToken {
  ResWordToken(String t) { 
    super(t, true);  testS("Read ResWordToken " + t); 
  }

  void check(Scanner s, String t) {
    if (! is(t)) s.error("Reserved word `" + t + "' expected.");
  }
  boolean is(String t) { return id.equals(t); }
}

class SyToken extends Token {
  String id;

  SyToken(String t) { 
    id = t;  testS("Read SyToken " + t); 
  }

  void check(Scanner s, String t) {
    if (! is(t)) s.error("Symbol `" + t + "' expected.");
  }

  String  image()      { return id; }
  boolean is(String t) { return id.equals(t); }
}

