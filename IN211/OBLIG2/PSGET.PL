#!/usr/bin/perl -w
#
# *************************************************
# file:		psget.pl
# what:		IN211 obligatorisk innlevering nummer 2
# author:	Frode Klevstul (frode@klevstul.com)
# started:	06 oktober 2001
# *************************************************

# --------------------------------------------
# use ting
# --------------------------------------------
use strict;																# bruker strict slik at alle variable m� deklareres

# --------------------------------------------
# deklarasjon av globale variable
# --------------------------------------------
my $source_file;														# ps filen det leses i fra
my $target_file = "out.ps";												# ps filen vi skal skrive til
my %print_pages;														# en hash tabell som inneholder sidene vi skal skrive ut
my $verbose;															# dersom $verbose s� skriv ut status til slutt (psget: 21 pages included; 17 pages omitted.)
my $debug = "yes";														# dersom denne parameteren er satt skrives det ut "status" til brukeren

# --------------------------------------------
# hoveddel
# --------------------------------------------
&getArguments;															# leser inn argumentene
&processPSFile;															# g�r igjennom og behandler ps filen og skriver resultatfil

# --------------------------------------------
# sub prosedyrer
# --------------------------------------------
sub getArguments{

	my $arg;															# argumentet legges i denne hjelpe variabelen
	my $p_check;														# variabel som brukes for � sjekke om '-p' er lest inn
	my $o_check;														# variabel som brukes for � sjekke om '-o' er lest inn
	my $i;																# hjelpevariabel i en for l�kke
	my $start_page_number;												# sidetallet vi skal begynne � lese ifra
	my $end_page_number;												# sidetallet vi skal lese til og med

	foreach $arg (@ARGV){												# l�kke som g�r igjennom alle argumentene brukeren har skrevet inn
		
		if ( $arg =~ m/^-help$/){										# dersom brukeren sender med '-help' som en parameter
			print "\n\n psget -v -o [output_file] -p [source_file]\n\n";
			print " --- parameters: ---\n";
			print " -v"				. "\t\t\t" 	. "verbose mode, optional\n";
			print " -o [filename]"	. "\t\t"	. "output file, optional, default 'out.ps'\n";
			print " -p"				. "\t\t\t"	. "must be on the format:\n";
			print "\t-px" 			. "\t\t\t"	. "get page 'x'\n";
			print "\t-px:y" 		. "\t\t\t"	. "get alle pages from page 'x' including 'y'\n\n";
			exit;
		}
		if ( $arg =~ m/^-/ || $o_check ){								# dersom argumentet starter med tegnet '-' (dvs en parameter/opsjon) eller det er etter en '-o' parameter
																		# gyldige parametere: "-p -v -o"
			if($o_check && $arg){										# dersom '-o' parameteren nettopp er lest inn og vi har et argument
				$o_check = "";
				$target_file = $arg;									# endrer $target_file
			}
			elsif($arg =~ m/^-p(\d+)$/ ||$arg =~ m/^-p(\d+):(\d+)$/){	# '-p' er p� formen '-p[tall]' eller '-p[tall1]:[tall2]'
				$p_check = "ok";										# ok, vi har mottat en '-p' som argument
				if($1 && $2){
					if($2 < $1){										# sjekker om [tall2]<[tall1], hvis det er tilfelle s� skrives det kun ut en side
						errorMsg("Argument '$arg': $2 < $1, only printing page $1");
						$start_page_number = $1;
					}
					else{												# mottatt to tall p� rikig form
						$start_page_number = $1;
						$end_page_number = $2;
					}
				}
				else{													# kun mottatt ett tall, dvs start = slutt siden
					$start_page_number = $1;
					$end_page_number = $1;
				}

				for($i = $start_page_number; $i <= $end_page_number; $i++){
					$print_pages{$i} = "ok";							# setter de sidene som er gitt ved argumenter hver gang en '-p' parameter behandles
				}

			}
			elsif($arg =~ m/^-v/){										# verbose mode er p�
				$verbose = "ok";
			}
			elsif($arg =~ m/^-o/){										# $o_check finnes: dvs at neste argument skal tolkes som filnavn
				$o_check = "ok";
			}
			else{														# argumentet er ikke gyldig
				errorMsg("Argument '$arg' skipped, illegal parameter.");
			}
			
		}
		else{															# arg starter ikke med '-' -> tolkes som et filnavn
			if (!$source_file){
				$source_file = $arg;									# ps filen som vi skal lese i fra settes
			}
			else{														# i dette tilfellet hopper vi over argumentet da filnavn allerede finnes
				errorMsg("Argument '$arg' skipped, source filname given more than one time.");
			}
		}	
	}

	if (!$p_check){														# alle argumentene er mottatt, men vi har ikke f�tt '-p' p� riktig form, vi avslutter
		errorMsg("Argumen '-p' not given or on wrong format.", "exit");	# slutter programmet dersom '-p' ikke er gitt
	}
	elsif(!$source_file){												# kildefilen er ikke oppgitt
		errorMsg("Source file not given, program exits", "exit");		# skriver ut en feilmelding og avslutter programmet
	}


	if ($debug){														# dersom $debug er satt s� skriver vi ut litt mer info til brukeren
		print "target_file: $target_file\n";
		print "source_file: $source_file\n";
		if($verbose){print "verbose\n"};
		foreach(keys(%print_pages)){
			print "get page " . $_ . "\n";
		}
		print "\n";
	}

}



sub processPSFile{

	my $line;															# variabel linjen leses til
	my $setup;															# variabel som sier oss om setup er lest inn
	my $trailer;														# variabel som sier om trailer er lest inn
	my $page;															# variabel som sier om �nsket page er lest inn
	my $begin_document = 0;												# teller som sier oss hvor mange "%%BeginDocument:" vi har lest inn (n�stede dokumenter)
	my $page_no = 0;													# teller til sidenummer som brukes i "%%Page: n k", 'n' er hva siden heter og 'k' er sidetallet (side nr k har navn n)
	my $no_pages_target = keys %print_pages;							# antall sider i det nye dokumentet som blir generert. Alternativ:  print "st�rrelsen er " . (scalar keys %print_pages) . "\n";
	my $no_pages_source;												# antall sider i opprinnelig dokumentet
	my $pages_obmitted;													# antall sider som er utelatt
	my $max_page_no;													# det h�yeste sidetallet gitt som argument
	my $min_page_no;													# det laveste sidetallet gitt som argument
	my @print_pages;													# m� bruke denne variabelen for � finne max av %print_pages hash'en: [0,$#print_pages]


	open(SOURCE_FILE, "<$source_file") || errorMsg("File not readable", "exit");				# �pner filen vi skal lese i fra
	open(TARGET_FILE, ">$target_file") || errorMsg("Could not open '$target_file'", "exit");	# �pner filen vi skal skrive til

	while ($line = <SOURCE_FILE>){										# g�r igjennom hele kildefilen
		

		if ($line =~ m/^%%Page:\s{1}/){									# frem til f�rste '%%Page' har vi setup
			$setup = "ok";
		}
		elsif ($line =~ m/^%%BeginDocument:\s{1}/){						# dersom vi har et dokument i et dokument etc...
			$begin_document++;											# teller for � holde rede p� n�stede dokuementer
		}
		elsif($line =~ m/^%%EndDocument$/){								# slutten p� et indre dokument
			$begin_document--;											# reduserer telleren med 1
		}


		if($begin_document == 0){										# dersom vi er inne i n�stede dokumenter skal vi ignorere start og stopp av "sider"
																		# denne testen vil da ikke sl� til, og $page vil ikke forandre status f�r vi er ferdig med indre/n�stede dokumenter
			
			if ($line =~ m/^%%Pages:\s{1}(\d+)$/){						# forandrer sideantall i det nye dokumentet som genereres

				($min_page_no, $max_page_no) = (sort { $a <=> $b } (keys %print_pages))[0,$#print_pages];		# finner det h�yeste og laveste sidetallet
				$no_pages_source = $1;
				if ( $max_page_no > $1){								# sjekker om det h�este sidetall oppgitt er h�yere enn totalt antall sider i kildefilen
					errorMsg("Page number out of boundary, 'given page > total no pages': '$max_page_no > $1'", "exit");
				}

				$line = "%%Pages: $no_pages_target\n";					# endrer linjen som spesifiserer antall sider i det nye dokumentet
			}
			elsif($line =~ m/^%%Page:\s{1}(\d+)\s(\d+)/){				# dersom vi starter en ny side

				if($print_pages{$2}){									# dersom vi skal skal ta vare p� siden i target filen
					$page_no++;											# �ker sidenummeret med 1 (begynner med 0)
					$line = "%%Page: $1 $page_no\n";					# endrer linjen som spesifiserer siden vi starter
					$page = "ok";										# setter variabelen som sier at vi har startet en ny side
				}
				else{													# nullstiller $page dersom vi ikke skal ta vare p� siden
					$page = "";
				}
			
			}
			elsif($line =~ m/^%%Trailer$/){								# trailer betyr slutten av dokumentet, vi skal da skrive resten til target filen
				$trailer = "ok";
			}
		}		

				
		if(!$setup || $page || $trailer){								# dersom vi ikke har kommet til setuplinjen, holder p� med en side eller har kommet til trailer/slutten av siden s�
			print TARGET_FILE $line;									# skriver vi til fil
		}


	}

	close(TARGET_FILE);													# lukker target filen
	close(SOURCE_FILE);													# lukker source filen

	if ($verbose){														# dersom verbose mode s� skriver vi ut litt info til brukeren
		$pages_obmitted = $no_pages_source - $no_pages_target;
		print "psget: $no_pages_target pages included; $pages_obmitted pages omitted.\n";
	}

}


sub errorMsg{															# feilmeldinger
	my ($text, $exit) = @_;												# mottar to variable
	print "ERROR: \"" . $text . "\"\n";									# skriv ut til skjerm
	print "type 'psget -help' for help\n\n";
	if($exit){															# og avslutt eventuelt programmet
		print "Exits...\n\n";
		exit;
	}
}

