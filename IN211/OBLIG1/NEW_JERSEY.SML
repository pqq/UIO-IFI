(* -= ============================================= =- *)
(* -= author:	Frode Klevstul (frode@klevstul.com) =- *)
(* -= started:	10.sep.01 							=- *)
(* -= what:		IN211 - oblig nr 1 - oppgave 1		=- *)
(* -= author:	Frode Klevstul (frode@klevstul.com) =- *)
(* -= ============================================= =- *)


(* ----------------------------------------------------------- *)
(* setter et element 'x' inn i listen 'l' i sortert rekkef�lge *)
(* ----------------------------------------------------------- *)
fun insert(l: int list, x:int): int list =
case l of nil	=> [x]
| y::r			=> if x<y then x::l else y::insert(r,x);


(* ------------------------------------------------------- *)
(* g�r igjennom alle elementene i en liste og sorterer dem *)
(* ------------------------------------------------------- *)
fun sort(l: int list): int list =
case l of nil 	=> nil
| x::r			=> insert(sort(r),x);


(* --------------------------------------------------------------------- *)
(* g�r igjennom alle elementene i en sortert liste og fjerner duplikater *)
(* --------------------------------------------------------------------- *)
fun remequals(l: int list): int list =
case l of nil	=> nil
| x::r			=> case r of nil	=> [x]
				   | y::r'			=> if x = y then remequals(r)
									   else x::remequals(r);


(* ---------------------------------------------------- *)
(* sl�r i sammen og sorterer en liste med liste av tall *)
(* ---------------------------------------------------- *)
fun merge(l: int list list): int list =
case l of nil	=> nil
| x::l			=> remequals(sort(x@merge(l)));


(* ------------------------------------------------------------------------------------------ *)
(* returnerer 'beginning' av listen (frem til at et elem. i listen er mindre enn det forrige) *)
(* ------------------------------------------------------------------------------------------ *)
fun split_b(l: int list): int list =
case l of nil	=> nil
| x::r			=> case r of nil	=> [x]
				   | y::r'			=> if x <= y then x::split_b(r) else [x];


(* ------------------------------------------------------------------------ *)
(* returnerer 'end' av listen (etter at et elem. er mindre enn det forrige) *)
(* ------------------------------------------------------------------------ *)
fun split_e(l: int list): int list =
case l of nil	=> nil
| x::r			=> case r of nil	=> nil
				   | y::r'			=> if x > y then r else split_e(r);


(* ------------------------------------------ *)
(* splitter opp en liste i en liste av lister *)
(* ------------------------------------------ *)
fun split(l: int list): int list list =
case l of nil	=> nil
| x::r			=> remequals(split_b(l))::split(split_e(l));


(* ----------------- *)
(* sorterer en liste *)
(* ----------------- *)
fun sort(l: int list):int list = merge(split(l));





(* -------------- *)
(* kj�rer en test *)
(* -------------- *)
val i = [1,5,3,6,8,8,3,5,8,3,6,6,9,4,6,9];
sort(i);



(* -= ============================================= =- *)
(* -= file:		sml_oblig1_2_new_in211.sml  =- *)
(* -= started:	14.okt.01 			    =- *)
(* -= what:		IN211 - oblig nr 1 - oppgave 2		=- *)
(* -= author:	Frode Klevstul (frode@klevstul.com) =- *)
(* -= ============================================= =- *)



fun gen2(h: int, l: int list list) =
case l of nil	=>	[h::nil]@[]
| x::r			=>	[h::x]@gen2(h,r);


(*	-- test p� kj�ring av "gen2"
	gen2(5, [[1],[2,4],[1,6,8]]);

	- gen2(5, [[1],[2,4],[1,6,8]]);
	val it = [[5,1],[5,2,4],[5,1,6,8],[5]] : int list list

*)

fun gen(l: int list) =
case l of nil	=>	nil
| x::r			=>	gen2(x, gen(r))@gen(r);


gen([1,2,3]);



(* **************************** *)
(* *** T�nnesen sin l�sning *** *)
(* **************************** *)
From: Andreas T�nnesen <andreto@student.matnat.uio.no>

- o o | DEL 2(n�tt) | o o -


Her skal det lages en funksjon som genererer alle mulige subsekvenser
av sekvensen gitt ved listen l.

For eksempel, skal

�� gen([1,3,5])


gi som resultat:

�� val it = [[],[1],[1,3],[1,3,5],[1,5],[3],[3,5],[5]] : int list list


Rekkef�lgen p� subsekvensene er ikke vesentlig.

Etter noen timers grubling og testing kom jeg frem til at dette egentlig ikke
trengte v�re s� innviklet som det s� ut til. Jeg hadde hele tiden brukt en slags
``fremmenfra og bakover''-tiln�rming til oppgaven selv om jeg brukte rekursivitet
- men etter hvert skj�nte jeg at det hele jo ble mye enklere hvis vi gikk til
verks fra bunnen!

Jeg satte meg opp f�lgende oversikt:


��� [1,�� 2,�� 3,�� 4,�� 5]
���� 1 - legges til kopier alle kombinasjoner av resten av listen
��������� 2 - legges til kopier alle kombinasjoner av resten av listen
�������������� 3 -� legges til kopier alle kombinasjoner av resten av listen
������������������� 4 - legges til kopier alle kombinasjoner av resten av listen
������������������������ 5 - legges til kopier alle kombinasjoner av resten av listen
��������������������������� []

Alts� hvert tall(element) i listen skulle legges til hver mulige kombinasjonsliste av
resten av listen. Kombinasjonslistene skulle selvf�lgelig ikke overskrives.

Jeg satte meg opp f�lgende oversikt:

� - hovedfunksjon - Denne skulle holde det hele sammen.
� - limefunksjon - Denne skulle legge det aktuelle tallet til hver av sekvensene
������������������ til resten av orginallisten(generert av neste funksjon).
� - subsekvensgenereringsfunksjon - Denne skulle generere alle mulige subsekvenser
����������������������������������� av en liste.

Etter en liten tenkepause kom jeg selvf�lgelig frem til at subsekvensgenereringsfunksjonen
var un�dvendig. Hvis limefunksjonen gikk rekursivt til verks fra bunnen av s� ville alle
kombinasjonene bli generert!


Jeg lagde limefunksjonen paste(h er tallet som skal limes p� alle listene i l):

��� fun paste(h: int, l: int list list):int list list = case l of [] => [h::nil]@[]
��� | x::r => [h::x]@paste(h, r);

eksempel p� kj�ring:
��� - paste(5, [[1,2,3],[2,3],[3]]);
��� val it = [[5,1,2,3],[5,2,3],[5,3],[5]] : int list list�


S� satte jeg meg ned for � lage hovedfunksjonen. F�rst pr�vde jeg med:

�� fun gen(l: int list):int list list = case l of [] => []
�� | x::r => paste(x, gen(r));

Som for meg var den intuitivt riktige l�sningen. Men la oss se hva som
skjedde.

kj�reeksempel:

��� - gen([1,3,4,5]);
��� val it = [[1,3,4,5],[1,3,4],[1,3],[1]] : int list list�

Riktignok ble alle l�sningene generert i det rekursive kallet, men bare f�rste
kallet p� paste ble returnert som endelig svar. Svaret ble at alt m�tte gj�res
"dobbelt opp". Alts� for hvert paste-kall s� m�tte hele listen gjennomg�es
rekursivt, s� m�tte returen konkateneres med et rekursivt gen-kall med
halen til orginalarrayet.

��� fun gen(l: int list):int list list = case l of [] => []
��� | x::r => paste(x, gen(r))@gen(r);

kj�reeksempel:

��� - gen([1,3,4,5]);
��� val it =
��� [[1,3,4,5],[1,3,4],[1,3,5],[1,3],[1,4,5],[1,4],[1,5],[1],[3,4,5],[3,4],[3,5],
��� [3],...] : int list list�

og selvf�lgelig:

��� - gen([1,3,5]);
��� val it = [[1,3,5],[1,3],[1,5],[1],[3,5],[3],[5]] : int list list������
�
..puh!

Det tomme arrayet kommer ikke med i svaret, dette synes jeg er litt mystisk
da det siste rekursive kallet(i dybden) skal v�re gen([]) som skal returnere [].
Om dette er fordi det burde ha blitt returnert [[]] eller [nil] vet ikke jeg,
men det roter til hele metoden n�r jeg pr�ver, og �rlig talt s� gidder jeg ikke
mer ;)

Alts� mitt endelige svar p� Del2 er:


����� fun gen(l: int list):int list list = case l of [] => []
����� | x::r => paste(x, gen(r))@gen(r);

����� fun paste(h: int, l: int list list):int list list = case l of [] => [h::nil]@[]
����� | x::r => [h::x]@paste(h, r);


Litt kronglete forklart dette men,men...
