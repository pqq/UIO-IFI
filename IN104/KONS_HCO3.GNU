# set terminal x11
set output 'Kons_HCO3.ps'
set xlabel 'Sulfat-kons., mol/L'
set ylabel 'HCO3-kons., mol/L'
set key 1.4e-4,1.3e-5
plot 'Kons_HCO3_A.num' title 'Res. A', 'Kons_HCO3_B.num' title 'Res. B'
 
