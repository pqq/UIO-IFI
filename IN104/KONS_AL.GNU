# set terminal x11
set output 'Kons_Al.ps'
set xlabel 'Sulfat-kons., mol/L'
set ylabel 'Aluminiumion-kons., mol/L'
set key 8e-5,1e-4
plot 'Kons_Al_A.num' title 'Res. A', 'Kons_Al_B.num' title 'Res. B'
 
